﻿using System;

namespace comp5002_9967618_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            //declare variables for name, number1, yes/no, number2, and number-total
            var name = "";
            var num1 = 0.00;
            var yesNo = "";
            var num2 = 0.00;
            var numTot = 0.00;

            //Print a welcome to the store (and make it look good)
            Console.WriteLine("*********************************");
            Console.WriteLine("****** WELCOME TO MY STORE ******");
            Console.WriteLine("*********************************");
            Console.WriteLine("");
            Console.WriteLine("=================================");

            Console.WriteLine("");

            //Have user enter name and store it as a variable(name) - print welcome with users name
            Console.WriteLine("Please enter your name");
            name = Console.ReadLine();
            Console.WriteLine($"Welcome {name}!");

            //Ask user to enter number, store input as variable num1
            Console.WriteLine("");
            Console.WriteLine("Please enter the price of your first item and press ENTER.");
            num1 = double.Parse(Console.ReadLine());

            //Ask user if they want to input another number. Store result as variable yesNo
            Console.WriteLine("Would you like to make another purchase?");
            Console.WriteLine("Make slection by typing Y or N and pressing ENTER");
            yesNo = Console.ReadLine();

            //if condition for Y(yes) 
            if (yesNo.Equals("Y"))
            {
                //true condition - repeat number entry but for variable num2
                Console.WriteLine("");
                Console.WriteLine("Please enter the price of your second item and press ENTER.");
                num2 = double.Parse(Console.ReadLine());
            }
            //determine total value by adding num1 and num2 and adding GST (15%)
            numTot = (num1+num2)*1.15;
            //Display variable numtot (number total) in dollar format
            Console.WriteLine("");
            Console.WriteLine("Thank you!");
            Console.WriteLine($"Your Total Purchase amount (Including GST) is ${numTot}");

            //display thankyou message and program close prompt
            Console.WriteLine("");
            Console.WriteLine("Thank you for shopping with us!");
            Console.WriteLine("Please come again!");

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
